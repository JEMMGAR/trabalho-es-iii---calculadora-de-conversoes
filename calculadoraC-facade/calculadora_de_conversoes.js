// Global variables
var inValor = document.getElementById("inValor");

function decimal() {
    let functionDecimal = new functionsDecimal();
    let valor = inValor.value.toLocaleUpperCase();
    
    checkEmptyField();

    var resultado = '';

    switch(inSelecao.value) {
        case '1': resultado = functionDecimal.decimal(valor); break;
        case '2': resultado = functionDecimal.octal(valor); break;
        case '3': resultado = functionDecimal.binario(valor); break;
        case '4': resultado = functionDecimal.hexadecimal(valor); break;
    }

    outResposta.value = resultado;
}
var btDecimal = document.getElementById("btDecimal");
btDecimal.addEventListener("click", decimal);


function octal() {
    let functionOctal = new functionsOctal();
    let valor = inValor.value.toLocaleUpperCase();

    checkEmptyField();

    let resultado = '';

    switch(inSelecao.value) {
        case '1': resultado = functionOctal.decimal(valor); break;
        case '2': resultado = functionOctal.octal(valor); break;
        case '3': resultado = functionOctal.binario(valor); break;
        case '4': resultado = functionOctal.hexadecimal(valor); break;
    }
    
    outResposta.value = resultado;
}
var btOctal = document.getElementById("btOctal");
btOctal.addEventListener("click", octal);


function binario() {
    var functionBinario = new functionsBinario();
    var valor = inValor.value.toLocaleUpperCase();

    checkEmptyField();

    var resultado = '';

    switch(inSelecao.value) {
        case '1': resultado = functionBinario.decimal(valor); break;
        case '2': resultado = functionBinario.octal(valor); break;
        case '3': resultado = functionBinario.binario(valor); break;
        case '4': resultado = functionBinario.hexadecimal(valor); break;
    }

    outResposta.value = resultado;
}
var btBinario = document.getElementById("btBinario");
btBinario.addEventListener("click", binario);


function hexadecimal() {
    let functionHexadecimal = new functionsHexadecimal();
    var valor = inValor.value.toLocaleUpperCase();

    var resultado = '';

    checkEmptyField();

    switch(inSelecao.value) {
        case '1': resultado = functionHexadecimal.decimal(valor); break;
        case '2': resultado = functionHexadecimal.octal(valor); break;
        case '3': resultado = functionHexadecimal.binario(valor); break;
        case '4': resultado = functionHexadecimal.hexadecimal(valor); break;
    }

    outResposta.value = resultado;
}
var btHexadecimal = document.getElementById("btHexadecimal");
btHexadecimal.addEventListener("click", hexadecimal);

// -------------------------------
// ---------- FUNCTIONS ----------
// -------------------------------

function informacaoIncorreta() {
    alert("Sua informação está incorreta!");
    resultado = "";
    inValor.focus();
    inValor.value = "";
}

function testeZeroToNine(i) {
    if (i != 1 && i != 2 && i != 3 && i != 4 && i != 5
            && i != 6 && i != 7 && i != 0) {
        return false;
    } else {
        return true;
    }
}

function testeZeroToF(i) {
    if (i != 1 && i != 2 && i != 3 && i != 4 && i != 5
            && i != 6 && i != 7 && i != 8 && i != 9 && i != 0
            && i != "A" && i != "B" && i != "C" && i != "D" && i != "E" && i != "F") {
        return false;
    } else {
        return true;
    }
}

function checkEmptyField() {
    if (inValor.value == "") {
        alert("Campo vazio!")
        inValor.focus()
        return;
    }
}

// -----------------------
// ------- OBJETOS -------
// -----------------------

class functionsDecimal {
    decimal(valor) {
        for (let i = 0; i < valor.length; i++) {
            if (!testeZeroToNine(valor[i])) {
                informacaoIncorreta();
                return false;
            }
        }
        return valor;
    }

    octal(valor) {
        for (let i = 0; i < valor.length; i++) {
            if (!testeZeroToNine(valor[i])) {
                informacaoIncorreta();
                return false;
            }
        }
        return parseInt(valor, 8).toString(10);
    }

    binario(valor) {
        for (let i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 0) {
                informacaoIncorreta();
                return false;
            }
        }
        return parseInt(valor, 2).toString(10);
    }

    hexadecimal(valor) {
        for (let i = 0; i < valor.length; i++) {
            if (!testeZeroToF(valor[i])) {
                informacaoIncorreta();
                return false;
            }
        }
        return parseInt(valor, 16).toString(10);
    }
}

class functionsOctal {
    decimal(valor) {
        for (let i = 0; i < valor.length; i++) {
            if (!testeZeroToNine(valor[i])) {
                informacaoIncorreta();
                return false;
            }
        }
        return parseInt(valor, 10).toString(8);
    }

    octal(valor) {
        for (let i = 0; i < valor.length; i++) {
            if (!testeZeroToNine(valor[i])) {
                informacaoIncorreta();
                return false;
            }
        }
        return parseInt(valor);
    }

    binario(valor) {
        for (let i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 0) {
                informacaoIncorreta();
                return false;
            }
        }
        return parseInt(valor, 2).toString(8);
    }

    hexadecimal(valor) {
        for (let i = 0; i < valor.length; i++) {
            if (!testeZeroToF(valor[i])) {
                informacaoIncorreta();
                return false;
            }
        }
        return parseInt(valor, 16).toString(8);
    }
}

class functionsBinario {
    decimal(valor) {
        for (let i = 0; i < valor.length; i++) {
            if (!testeZeroToNine(valor[i])) {
                informacaoIncorreta();
                return false;
            }
        }
        return parseInt(valor, 10).toString(2);
    }

    octal(valor) {
        for (let i = 0; i < valor.length; i++) {
            if (!testeZeroToNine(valor[i])) {
                informacaoIncorreta();
                return false;
            }
        }
        return parseInt(valor, 8).toString(2);
    }

    binario(valor) {
        for (let i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 0) {
                informacaoIncorreta();
                return false;
            }
        }
        return valor;
    }

    hexadecimal(valor) {
        for (let i = 0; i < valor.length; i++) {
            if (!testeZeroToF(valor[i])) {
                informacaoIncorreta();
                return false;
            }
        }
        return parseInt(valor, 16).toString(2);
    }
}

class functionsHexadecimal {
    decimal(valor) {
        for (let i = 0; i < valor.length; i++) {
            if (!testeZeroToNine(valor[i])) {
                informacaoIncorreta();
                return false;
            }
        }
        return parseInt(valor, 10).toString(16).toLocaleUpperCase();
    }

    octal(valor) {
        for (let i = 0; i < valor.length; i++) {
            if (!testeZeroToNine(valor[i])) {
                informacaoIncorreta();
                return false;
            }
        }
        return parseInt(valor, 8).toString(16).toLocaleUpperCase();
    }

    binario(valor) {
        for (let i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 0) {
                informacaoIncorreta();
                return false;
            }
        }
        return parseInt(valor, 2).toString(16).toLocaleUpperCase();
    }

    hexadecimal(valor) {
        for (let i = 0; i < valor.length; i++) {
            if (!testeZeroToF(valor[i])) {
                informacaoIncorreta();
                return false;
            }
        }
        return valor.toLocaleUpperCase();
    }
}