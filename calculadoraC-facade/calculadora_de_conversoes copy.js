var inValor = document.getElementById("inValor");

function decimal() {
    var valor = inValor.value.toLocaleUpperCase();

    if (inValor.value == "") {
        alert("Campo vazio!")
        inValor.focus();
        return;
    }

    var resultado;

    if (inSelecao.value == 1) {
        for (i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 2 && valor[i] != 3 && valor[i] != 4 && valor[i] != 5
                && valor[i] != 6 && valor[i] != 7 && valor[i] != 8 && valor[i] != 9 && valor[i] != 0) {
                alert("Sua informação está incorreta!");
                resultado = "";
                inValor.focus();
                inValor.value = "";
                return;
                break;
            } else {
                resultado = valor;
            }
        }
    } else if (inSelecao.value == 2) {
        for (i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 2 && valor[i] != 3 && valor[i] != 4 && valor[i] != 5
                && valor[i] != 6 && valor[i] != 7 && valor[i] != 0) {
                alert("Sua informação está incorreta!");
                resultado = "";
                inValor.focus();
                inValor.value = "";
                return;
                break
            } else {
                resultado = parseInt(valor, 8).toString(10);
            }
        }
    } else if (inSelecao.value == 3) {
        for (i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 0) {
                alert("Sua informação está incorreta!");
                resultado = "";
                inValor.focus();
                inValor.value = "";
                return;
                break
            } else {
                resultado = parseInt(valor, 2).toString(10);
            }
        }
    } else if (inSelecao.value == 4) {
        for (i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 2 && valor[i] != 3 && valor[i] != 4 && valor[i] != 5
                && valor[i] != 6 && valor[i] != 7 && valor[i] != 8 && valor[i] != 9 && valor[i] != 0
                && valor[i] != "A" && valor[i] != "B" && valor[i] != "C" && valor[i] != "D" && valor[i] != "E" && valor[i] != "F") {
                alert("Sua informação está incorreta!");
                resultado = "";
                inValor.focus();
                inValor.value = "";
                return;
                break
            } else {
                resultado = parseInt(valor, 16).toString(10);
            }
        }
    }
    outResposta.value = resultado;
}
var btDecimal = document.getElementById("btDecimal");
btDecimal.addEventListener("click", decimal);


function octal() {
    // var inValor = document.getElementById("inValor");
    var valor = inValor.value.toLocaleUpperCase();

    if (inValor.value == "") {
        alert("Campo vazio!")
        inValor.focus();
        return;
    }

    var resultado;

    if (inSelecao.value == 1) {
        for (i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 2 && valor[i] != 3 && valor[i] != 4 && valor[i] != 5
                && valor[i] != 6 && valor[i] != 7 && valor[i] != 8 && valor[i] != 9 && valor[i] != 0) {
                alert("Sua informação está incorreta!");
                resultado = "";
                inValor.focus();
                inValor.value = "";
                return;
                break;
            } else {
                resultado = parseInt(valor, 10).toString(8);
            }
        }

    } else if (inSelecao.value == 2) {
        for (i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 2 && valor[i] != 3 && valor[i] != 4 && valor[i] != 5
                && valor[i] != 6 && valor[i] != 7 && valor[i] != 0) {
                alert("Sua informação está incorreta!");
                resultado = "";
                inValor.focus();
                inValor.value = "";
                return;
                break
            } else {
                resultado = parseInt(valor);
            }
        }

    } else if (inSelecao.value == 3) {
        for (i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 0) {
                alert("Sua informação está incorreta!");
                resultado = "";
                inValor.focus();
                inValor.value = "";
                return;
                break
            } else {
                resultado = parseInt(valor, 2).toString(8);
            }
        }

    } else if (inSelecao.value == 4) {
        for (i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 2 && valor[i] != 3 && valor[i] != 4 && valor[i] != 5
                && valor[i] != 6 && valor[i] != 7 && valor[i] != 8 && valor[i] != 9 && valor[i] != 0
                && valor[i] != "A" && valor[i] != "B" && valor[i] != "C" && valor[i] != "D" && valor[i] != "E" && valor[i] != "F") {
                alert("Sua informação está incorreta!");
                resultado = "";
                inValor.focus();
                inValor.value = "";
                return;
                break
            } else {
                resultado = parseInt(valor, 16).toString(8);
            }
        }
    }
    outResposta.value = resultado;
}
var btOctal = document.getElementById("btOctal");
btOctal.addEventListener("click", octal);


function binario() {
    // var inValor = document.getElementById("inValor");
    var valor = inValor.value.toLocaleUpperCase();

    if (inValor.value == "") {
        alert("Campo vazio!")
        inValor.focus();
        return;
    }

    var resultado;

    if (inSelecao.value == 1) {
        for (i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 2 && valor[i] != 3 && valor[i] != 4 && valor[i] != 5
                && valor[i] != 6 && valor[i] != 7 && valor[i] != 8 && valor[i] != 9 && valor[i] != 0) {
                alert("Sua informação está incorreta!");
                resultado = "";
                inValor.focus();
                inValor.value = "";
                return;
                break;
            } else {
                resultado = parseInt(valor, 10).toString(2);
            }
        }

    } else if (inSelecao.value == 2) {
        for (i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 2 && valor[i] != 3 && valor[i] != 4 && valor[i] != 5
                && valor[i] != 6 && valor[i] != 7 && valor[i] != 0) {
                alert("Sua informação está incorreta!");
                resultado = "";
                inValor.focus();
                inValor.value = "";
                return;
                break
            } else {
                resultado = parseInt(valor, 8).toString(2);
            }
        }

    } else if (inSelecao.value == 3) {
        for (i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 0) {
                alert("Sua informação está incorreta!");
                resultado = "";
                inValor.focus();
                inValor.value = "";
                return;
                break
            } else {
                resultado = valor;
            }
        }

    } else if (inSelecao.value == 4) {
        for (i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 2 && valor[i] != 3 && valor[i] != 4 && valor[i] != 5
                && valor[i] != 6 && valor[i] != 7 && valor[i] != 8 && valor[i] != 9 && valor[i] != 0
                && valor[i] != "A" && valor[i] != "B" && valor[i] != "C" && valor[i] != "D" && valor[i] != "E" && valor[i] != "F") {
                alert("Sua informação está incorreta!");
                resultado = "";
                inValor.focus();
                inValor.value = "";
                return;
                break
            } else {
                resultado = parseInt(valor, 16).toString(2);
            }
        }
    }
    outResposta.value = resultado;
}
var btBinario = document.getElementById("btBinario");
btBinario.addEventListener("click", binario);


function hexadecimal() {
    // var inValor = document.getElementById("inValor");
    var valor = inValor.value.toLocaleUpperCase();

    var resultado;

    if (inValor.value == "") {
        alert("Campo vazio!")
        inValor.focus();
        return;
    }

    if (inSelecao.value == 1) {
        for (i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 2 && valor[i] != 3 && valor[i] != 4 && valor[i] != 5
                && valor[i] != 6 && valor[i] != 7 && valor[i] != 8 && valor[i] != 9 && valor[i] != 0) {
                alert("Sua informação está incorreta!");
                resultado = "";
                inValor.focus();
                inValor.value = "";
                return;
                break;
            } else {
                resultado = parseInt(valor, 10).toString(16).toLocaleUpperCase();
            }
        }

    } else if (inSelecao.value == 2) {
        for (i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 2 && valor[i] != 3 && valor[i] != 4 && valor[i] != 5
                && valor[i] != 6 && valor[i] != 7 && valor[i] != 0) {
                alert("Sua informação está incorreta!");
                resultado = "";
                inValor.focus();
                inValor.value = "";
                return;
                break
            } else {
                resultado = parseInt(valor, 8).toString(16).toLocaleUpperCase();
            }
        }

    } else if (inSelecao.value == 3) {
        for (i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 0) {
                alert("Sua informação está incorreta!");
                resultado = "";
                inValor.focus();
                inValor.value = "";
                return;
                break
            } else {
                resultado = parseInt(valor, 2).toString(16).toLocaleUpperCase();
            }
        }

    } else if (inSelecao.value == 4) {
        for (i = 0; i < valor.length; i++) {
            if (valor[i] != 1 && valor[i] != 2 && valor[i] != 3 && valor[i] != 4 && valor[i] != 5
                && valor[i] != 6 && valor[i] != 7 && valor[i] != 8 && valor[i] != 9 && valor[i] != 0
                && valor[i] != "A" && valor[i] != "B" && valor[i] != "C" && valor[i] != "D" && valor[i] != "E" && valor[i] != "F") {
                alert("Sua informação está incorreta!");
                resultado = "";
                inValor.focus();
                inValor.value = "";
                return;
                break
            } else {
                resultado = valor.toLocaleUpperCase();
            }
        }
    }
    outResposta.value = resultado;
}
var btHexadecimal = document.getElementById("btHexadecimal");
btHexadecimal.addEventListener("click", hexadecimal);

function informacaoIncorreta() {
    alert("Sua informação está incorreta!");
    resultado = "";
    inValor.focus();
    inValor.value = "";
}